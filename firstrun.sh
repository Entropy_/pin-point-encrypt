#! /bin/bash

adduser -Z axon
adduser -Z neuron

mkdir /access-point/

cp axon /access-point/axon

chown -R axon /access-point/axon
chown neuron /access-point/axon



echo y | dnf install gnupg policycoreutils-python-utils 

#start neuron config

cp .access.axon /home/neuron/.access.axon
cp .logout.axon /home/neuron/.logout.axon
chown -R neuron /home/neuron
chmod +x /home/neuron/.access.axon
chmod +x /home/neuron/.logout.axon

echo /home/neuron/.access.axon >>  /home/neuron/.bash_profile

echo /home/neuron/.logout.axon >> /home/neuron/.bash_logout
#end neuron config

#axon config
cp -R axon /home/axon/axon
chown -R axon /home/axon

#copy the neuron's key to axon
cp /home/neuron/.ssh/id_rsa.pub /home/axon/.ssh/neuron.pub
