boolean -D
login -D
interface -D
user -D
port -D
node -D
fcontext -D
module -D
ibendport -D
ibpkey -D
boolean -m -1 domain_can_mmap_files
boolean -m -1 ssh_sysadm_login
boolean -m -1 use_virtualbox
login -a -s axon_u -r 's0-s6:c0.c6' axon
login -a -s neuron_u -r 's0-s5:c0.c5' neuron
user -a -L s0 -r s0-s6:c0.c6 -R 'staff_r user_r' axon_u
user -a -L s5 -r s0-s5:c0.c5 -R 'user_r' neuron_u
user -a -L s0 -r s0-s15:c0.c1023 -R 'auditadm_r system_r sysadm_r secadm_r' root
user -a -L s0 -r s0-s15:c0.c1023 -R 'auditadm_r system_r staff_r sysadm_r' staff_u
user -a -L s0 -r s0-s15:c0.c1023 -R 'auditadm_r secadm_r sysadm_r system_r staff_r' system_u
user -a -L s6 -r s0-s6:c0.c6 -R 'user_r' user_u
fcontext -a -f a -t user_home_dir_t -r 's0' '/access'
fcontext -a -f a -t user_home_dir_t -r 's0' '/access-point'


for example, to add the last line, you would issue
semanage fcontext -a -t user_home_dir_t -s axon_u -r s0 /access-point
from the command line, as root
semanage fcontext -a -f d -t user_home_dir_t -r 's0' '/access-point'

