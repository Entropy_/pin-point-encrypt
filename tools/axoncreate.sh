#! /bin/bash

mkdir -v /axon

chown -vR axon /axon

echo 'Changing context of /axon'
semanage fcontext -a -f d -t user_home_dir_t -r 's0' -s axon_u '/axon'

echo "Applying changes to /axon"
restorecon -FRv /axon

echo "Changing to axon user"
echo "tacos" | su axon


echo "Moving into the /axon directory"
cd /axon

echo "Looking around..."
ls -Zd /axon

echo "Poking the body."
touch  made-it.txt

echo 'MADE IT!'
