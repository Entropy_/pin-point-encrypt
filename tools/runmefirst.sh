#! /bin/bash
dnf install gnupg policycoreutils-python-utils setools-console

adduser -Z axon
adduser -Z neuron

cp -R keys /home/axon/keys
cp -R keys /home/neuron/keys

runuser -l axon gpg --import ../keys/SSPaxonpriv.gpg
runuser -l axon gpg --import ../keys/SSPaxonpub.gpg
runuser -l axon gpg --import ../keys/SSPneuronpriv.gpg
runuser -l axon gpg --import ../keys/SSPneuronpub.gpg

runuser -l neuron gpg --import ../keys/SSPaxonpub.gpg
runuser -l neuron gpg --import ../keys/SSPneuronpriv.gpg
runuser -l neuron gpg --import ../keys/SSPneuronpub.gpg



runuser -l neuron ssh-keygen
runuser -l axon ssh-keygen

echo "Remember to copy the private keys from"
echo "/home/axon/.ssh and /home/neuron/.ssh"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
