***UNDER CONSTRUCTION***

This will be a script that takes in a list of files,
and encrypts/decrypts them in accordance with a layout,
which allows for a "topological" encrypt.

The name comes from the fact that if you take a pin
and hold it at arm's length, the entirety of ONE
of the hubble space telecope's galaxy fields rests
behind that one pinhead.

So I'm thinking a list of files stored in an 
encrypted txt, with a super password. The twist,
is that there are different levels of access.
So you can have access to a $GROUP, at a certain
level. When the access is $PASSED, the script
unbundles the $LAYOUT and sets read or write
permissions as defined in the $SETUP(which will
be readable only by system-level processes)

GNUpg will provide the basis of our encryption
keys. Other checks will include simple security
questions. This will be convoluted AF, on purpose.

NOTES ON $PASSED:
  Access will be PASSED when the script is called
  with an argument password.
  
NOTES ON $SETUP:
  The $SETUP file will be installed in a system place,
  with access only from system level protocols triggered
  by different password-types.

NOTES ON #PASSWORD-TYPES:
  There will be password requirements and a heirarchy.
  Passwords with 4 digits will have the least access and
  12 more. SSH key logins will be the highest level.

NOTES ON FUN THINGS:
  users will have scripts to create/destroy their setups.
  Triggered by passwords, and created/destroyed before login.
  This might make for a single user only process. But I'm
  fine with that idea.
  From the outside, or to someone who had never seen it before
  it will look like a simple system with only a root and a single-user.
  
  Some things to think about, if another user tries to log in
  while another user is on the system, it will refuse access.
  This is a problem if you think of abuse. To lock everyone else
  out, all you have to do is stay logged in.
  
  I propose a "house-key" system. If another user is logged in,
  you get refused and the system will send you an email asking
  if you'd like to use your key to boot the other user.
  Some features include keeping the system in the state that they had it in,
  to provide further investiation at the time of access.
  Another feature would be to simply wipe clean all changes made
  from the other user.
  The hybrid usage would be to send an echo to the logged in user.
  For fun it will be a simple "KNOCK" from a user with a specified
  key. If the user gets kicked, it will echo, "I AM THE ONE WHO KNOCKS"
  at logoff.

NOTES ON $LAYOUT:
  The script will have to unbundle the stack from
  an encrypted directory containing individually
  locked files, all only read/write by system level.
  The end result will be put into /pin-point-encrypt/access-point/
  Each file will have a line that defines its position
  in the overall layout. When the file is re-read
  by the script for re-packing, it will check the
  permissions and that location to make sure they're in
  line with when it was unpacked and ready for reading.
  If not, it will barf an error and lock down the access
  point. --I will have to make sure to create a dummy package,
  I will fuck this up over and over I'm sure--

NOTES ON $GROUP:
  Groups will have predefined layout structures,
  and I will have to make up a few possible systems.
  Webpages, the-terrible-download seems like an intricate
  enough system that I can just blast to hell as many
  times as I like.

NOTES ON $INTERFACE:
  As much as I enjoy toast, I think I will shy away
  from making this pretty. I enjoy the linux command
  line interface enough right now. If that changes
  in the future, I will re-assess.
  re-assessing. the file layoutplanning.md has an example
  of what I'd like a user with AXON level priviledges would
  see. There would never be an AXON level human.



It will use GNUpg, and be written in BASH.

First question, what kind of key do I use?

"Any key size beyond 2048 bits is the equivalent of buying a red sports car to woo girls." --Stackexchange

4096 bit Elgamal it is!

aw, our only choice is 3072 bit Elgamal

First thought, I don't want to publish my keys.

We will have to workout how many keys we should use
for a specified setup.

Let's seee

system
root
basic basic basic

so five keys, five passwords, five users.

the system account will have to be created first
then the layout file with an example layout

An AXON view follows  
![](axonview.png)


access-point will be the root of all operations, so if ALPHA-dendrite logs
in, the layout would look like this.  
![](ALPHA-DENDRITEview.png)



if NEURON logs in, it would look like this  
![](NEURONview.png)  
			
However the system AXON has access to every layer on the spectrum.
With a password for read priviledges and one for write. We will
disable the write key and remove all access beyond read once
the system has been initiated and created.